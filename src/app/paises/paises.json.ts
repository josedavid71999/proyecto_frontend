import { Pais } from './pais';

export const PAISES : Pais[] = [
  {  nombre: 'Peru',
    region: 'Americas',
    subregion: 'South America',
    languages : 'Español',
    currencies : 'PEN',
    coatOfArms : 'https://mainfacts.com/media/images/coats_of_arms/pe.svg'
  },
  {  nombre: 'Federative Republic of Brazil',
    region: 'Americas',
    subregion: 'South America',
    languages : 'Portuguese',
    currencies : 'BRL',
    coatOfArms : 'https://mainfacts.com/media/images/coats_of_arms/pe.svg'
  }
];
