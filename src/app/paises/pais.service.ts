import { throwError,Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { PAISES } from './paises.json';
import { Pais } from './pais';
import { HttpClient } from '@angular/common/http';
import { catchError,map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PaisService {

  private urlEndPointCountry : string = 'http://localhost:8080/api/paises/';
  constructor(private http: HttpClient) { }

  getCountrys(name: string): Observable<Pais[]> {
    return this.http.get(this.urlEndPointCountry + name).pipe(
      map(response => {
        return response as Pais[];
      }),
      catchError(error => {
        return throwError(error);
      })
    );
  }
}
