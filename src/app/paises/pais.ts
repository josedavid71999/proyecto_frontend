export class Pais {
  nombre: string;
  region: string;
  subregion: string;
  languages : string;
  currencies : string;
  coatOfArms : string;
}
