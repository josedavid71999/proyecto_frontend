import { PaisService } from './pais.service';

import { Component } from '@angular/core';
import { Pais } from './pais';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-paises',
  templateUrl: './paises.component.html',
  styleUrls: ['./paises.component.css']
})
export class PaisesComponent{
  Paises : Pais[];
  countryName: string;
  isValidInput : boolean = true;
  isLoading: boolean = false;
  contador: number = 1;

  constructor(private paisService :PaisService){}

  ngOnInit(): void {
    this.Paises = null;
  }

  getCountrys():void{
    this.isLoading = true;
    this.isValidInput = true;
    this.paisService.getCountrys(this.countryName).subscribe(
      response => {
        this.Paises = response;
        this.isValidInput = false;
        this.isLoading = false;
      },
      error => {
        console.error('Error:', error.message);
        this.Paises = null;
        this.isValidInput = false;
        this.isLoading = false;
          const Toast = Swal.mixin({
            background : '#ff6d88',
            color : 'white',
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
            didOpen: (toast) => {
              toast.addEventListener('mouseenter', Swal.stopTimer)
              toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
          })

          Toast.fire({
            icon: 'warning',
            title: 'No se encontró resultado a la búsqueda'
          })
      }
    );
  }

  validInput(){
    if(this.countryName === null || this.countryName === '' || this.countryName === undefined){
      this.isValidInput = true;
    }else{
      this.isValidInput = false;
    }
  }



}
